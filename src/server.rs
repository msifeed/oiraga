extern crate mio;

use mio::*;
use mio::udp::*;
use std::str;

const LISTENER: Token = Token(0);
const SENDER: Token = Token(1);

struct OiragaServer {
    listener: UdpSocket,
    sender: UdpSocket
}

impl OiragaServer {
    fn new(listener: UdpSocket, sender: UdpSocket) -> OiragaServer {
        OiragaServer {
            listener: listener,
            sender: sender
        }
    }
}

impl Handler for OiragaServer {
    type Timeout = usize;
    type Message = ();

    fn ready(&mut self, event_loop: &mut EventLoop<OiragaServer>, token: Token, events: EventSet) {
        if events.is_readable() {
            match token {
                LISTENER => {
                    let mut buf: [u8; 1024] = [0; 1024];

                    let bytes_cnt: usize = self.listener.recv_from(&mut buf).unwrap().unwrap().0;
                    let slice: &[u8] = &buf[0 .. bytes_cnt];
                    let rcv_msg = str::from_utf8(&slice).unwrap();

                    println!("Read {:?}", rcv_msg);
                },
                _ => {}
            }
        }

        // if events.is_writable() {
        //     match token {
        //         SENDER => {
        //             let addr = self.listener.local_addr().unwrap();
        //             let msg_bytes = &self.msg.as_bytes();

        //             println!("send {} bytes", &msg_bytes.len());
        //             self.sender.send_to(&msg_bytes, &addr).unwrap();
        //         },
        //         _ => {}
        //     }
        // }
    }
}

fn main() {
    let server_addr = "127.0.0.1:10000".parse().unwrap();
    let client_addr = "0.0.0.0:0".parse().unwrap();

    let server_sock = UdpSocket::bound(&server_addr).unwrap();
    let client_sock = UdpSocket::bound(&client_addr).unwrap();

    let mut event_loop = EventLoop::new().unwrap();
    event_loop.register(&server_sock, LISTENER, EventSet::readable(), PollOpt::edge()).unwrap();
    event_loop.register(&client_sock, SENDER, EventSet::writable(), PollOpt::edge()).unwrap();

    println!("Starting event loop");
    event_loop.run(&mut OiragaServer::new(server_sock, client_sock)).unwrap();
}