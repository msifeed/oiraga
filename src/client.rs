extern crate mio;

use mio::*;
use mio::udp::*;
use std::io;
use std::thread;

const LISTENER: Token = Token(0);
const SENDER: Token = Token(1);

struct OiragaClientHandler {
    listener_sock: UdpSocket,
    sender_sock: UdpSocket
}

impl OiragaClientHandler {
    fn new(listener_sock: UdpSocket, sender_sock: UdpSocket) -> OiragaClientHandler {
        OiragaClientHandler {
            listener_sock: listener_sock,
            sender_sock: sender_sock
        }
    }

    fn send_msg(&self, msg: &String) {
        println!("Sending {} ...", msg);
    }
}

impl Handler for OiragaClientHandler {
    type Timeout = usize;
    type Message = String;

    // fn ready(&mut self, event_loop: &mut EventLoop<OiragaClientHandler>, token: Token, events: EventSet) {
    // }

    fn notify(&mut self, event_loop: &mut EventLoop<OiragaClientHandler>, msg: String) {
        // assert_eq!(msg, 123);
        // event_loop.shutdown();
        println!("Send {:?}", msg);
        let addr = "127.0.0.1:10000".parse().unwrap();
        self.sender_sock.send_to(msg.as_bytes(), &addr);
    }
}

fn main() {
    let listener_addr = "127.0.0.1:10100".parse().unwrap();
    let sender_addr = "0.0.0.0:0".parse().unwrap();

    let listener_sock = UdpSocket::bound(&listener_addr).unwrap();
    let sender_sock = UdpSocket::bound(&sender_addr).unwrap();

    let mut event_loop: EventLoop<OiragaClientHandler> = EventLoop::new().unwrap();
    let sender = event_loop.channel();
    event_loop.register(&listener_sock, LISTENER, EventSet::readable(), PollOpt::edge()).unwrap();
    event_loop.register(&sender_sock, SENDER, EventSet::writable(), PollOpt::edge()).unwrap();

    println!("Starting net thread...");
    thread::spawn(move || {
        event_loop.run(&mut OiragaClientHandler::new(listener_sock, sender_sock)).unwrap();
    });

    println!("Running input loop...");
    loop {
        let mut msg_input = String::new();
        io::stdin().read_line(&mut msg_input).ok().expect("Failed to read input");
        sender.send(msg_input);
    }
 
}
